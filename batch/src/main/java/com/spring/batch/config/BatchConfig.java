package com.spring.batch.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@EnableBatchProcessing
@Configuration
public class BatchConfig {
	//constructor de trabajos
	@Autowired
	public JobBuilderFactory jobBuilderFactory;
	//constructor de pasos
	@Autowired
	public StepBuilderFactory stepBuilderFactory;

	//metodo de preparacion de un paso
	@Bean
	public Step packageItemStep() {
		return this.stepBuilderFactory.get("packageItemStep").tasklet(new Tasklet() {

			@Override
			public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
				System.out.print("El articulo ha sido empaquetado...");
				return RepeatStatus.FINISHED;
			}
			
		}).build();
	}
	
	//metodo para lanzar un job
	@Bean
	public Job deliverPackageJob() {
		return  this.jobBuilderFactory.get("trabajo entrega de paquete").start(packageItemStep()).build();
	}

}
